#CONFIG="config_settings.ini"
CONFIG="config_high_features.ini"
#Datasets
#FILE_NAME="Boat_Poles_HIGH_NOR"         #_FUS  or _BCK  _NOR
#FILE_NAME="Boat_0915_1st_HIGH_NOR"         #_FUS  or _BCK  _NOR
FILE_NAME="River_0809_rec_sd_jtty_HIGH_NOR"         #_FUS  or _BCK  _NOR

#done
#FILE_NAME="Woodman_0910_snd_crcl_HIGH_NOR"         #_FUS  or _BCK  _NOR
#FILE_NAME="Omeo_0910_drv_wreck_HIGH_NOR"         #_FUS  or _BCK  _NOR
#FILE_NAME="River_1409_crcl_longer_HIGH_NOR"         #_FUS  or _BCK  _NOR
#FILE_NAME="Marina_0809_eight_HIGH_NOR"         #_FUS  or _BCK  _NOR
# Filters
#FILE_NAME="Woodman_0810_snd_crcl_HIGH_FUS"         #_FUS  or _BCK  _NOR
#FILE_NAME="Woodman_0810_snd_crcl_HIGH_BCK"         #_FUS  or _BCK  _NOR
#FILE_NAME="Marina_0809_eight_HIGH_FUS"         #_FUS  or _BCK  _NOR
#FILE_NAME="Marina_0809_eight_HIGH_BCK"         #_FUS  or _BCK  _NOR
#FILE_NAME="River_0809_rec_jtty_HIGH_FUS"         #_FUS  or _BCK  _NOR
#FILE_NAME="River_0809_rec_jtty_HIGH_BCK"         #_FUS  or _BCK  _NOR
#FILE_NAME="Marina_1409_eight_gnd_HIGH_FUS"         #_FUS  or _BCK  _NOR
#FILE_NAME="Marina_1409_eight_gnd_HIGH_BCK"         #_FUS  or _BCK  _NOR

#Datasets
#FOLDER="/media/franco/850Evo/FH/Datasets/Boat/2017-09-15/Poles/"   #/ ENDS /
#FOLDER="/media/franco/850Evo/FH/Datasets/Boat/2017-09-15/1st_spot/"   #/ ENDS /
FOLDER="/media/franco/850Evo/FH/Datasets/River_at_Bicton/08-09-17/Rectangle_one_side_along_jetty/"   #/ ENDS /


#done
#FOLDER="/media/franco/850Evo/FH/Datasets/Woodman_point/2017-09-10/Sand_circle/"   #/ ENDS /
#FOLDER="/media/franco/850Evo/FH/Datasets/Omeo_wreck/2017-09-10/Drive_along_wreck/"   #/ ENDS /
#FOLDER="/media/franco/850Evo/FH/Datasets/River_at_Bicton/14-09-17/Circle_longer/"   #/ ENDS /
#FOLDER="/media/franco/850Evo/FH/Datasets/Marina_in_Fremantle/08-09-17/Eight/"   #/ ENDS /
# filters
#FOLDER="/media/franco/850Evo/FH/Datasets_Fusion/Woodman_0909_s_crcl/"   #/ ENDS /
#FOLDER="/media/franco/850Evo/FH/Datasets_back/Woodman_0910_snd_crcl/"   #/ ENDS /
#FOLDER="/media/franco/850Evo/FH/Datasets_Fusion/Marina_0809_eight/"   #/ ENDS /
#FOLDER="/media/franco/850Evo/FH/Datasets_back/Marina_0809_eight/"   #/ ENDS /
#FOLDER="/media/franco/850Evo/FH/Datasets_Fusion/River_0809_rec_sd_jtty/"   #/ ENDS /
#FOLDER="/media/franco/850Evo/FH/Datasets_back/River_0809_rec_jetty/"   #/ ENDS /
#FOLDER="/media/franco/850Evo/FH/Datasets_Fusion/Marina_14_09_eight_gnd/"   #/ ENDS /
#FOLDER="/media/franco/850Evo/FH/Datasets_back/Marina_1409_eight_gnd/"   #/ ENDS /


echo "$FILENAME" &&
echo "$FILENAME" &&
echo "$FILENAME" &&

#~/code/uw_image_enhance/find-object-0.6.0/find-object/bin/find_object --config ~/code/uw_image_enhance/find-object-0.6.0/find-object/custom/$CONFIG --Feature2D/1Detector "4:Dense;Fast;GFTT;MSER;ORB;SIFT;Star;SURF;BRISK;AGAST;KAZE;AKAZE" --Feature2D/2Descriptor "1:Brief;ORB;SIFT;SURF;BRISK;FREAK;KAZE;AKAZE;LUCID;LATCH;DAISY" --Camera/5mediaPath $FOLDER &&

~/code/uw_image_enhance/find-object-0.6.0/find-object/bin/find_object --no_display --config ~/code/uw_image_enhance/find-object-0.6.0/find-object/custom/$CONFIG --Feature2D/1Detector "4:Dense;Fast;GFTT;MSER;ORB;SIFT;Star;SURF;BRISK;AGAST;KAZE;AKAZE" --Feature2D/2Descriptor "1:Brief;ORB;SIFT;SURF;BRISK;FREAK;KAZE;AKAZE;LUCID;LATCH;DAISY" --Camera/5mediaPath $FOLDER > "$FILE_NAME"_ORB.txt &

~/code/uw_image_enhance/find-object-0.6.0/find-object/bin/find_object --no_display --config ~/code/uw_image_enhance/find-object-0.6.0/find-object/custom/$CONFIG --Feature2D/1Detector "5:Dense;Fast;GFTT;MSER;ORB;SIFT;Star;SURF;BRISK;AGAST;KAZE;AKAZE" --Feature2D/2Descriptor "2:Brief;ORB;SIFT;SURF;BRISK;FREAK;KAZE;AKAZE;LUCID;LATCH;DAISY" --Camera/5mediaPath $FOLDER > "$FILE_NAME"_SIFT.txt &

~/code/uw_image_enhance/find-object-0.6.0/find-object/bin/find_object --no_display --config ~/code/uw_image_enhance/find-object-0.6.0/find-object/custom/$CONFIG --Feature2D/1Detector "7:Dense;Fast;GFTT;MSER;ORB;SIFT;Star;SURF;BRISK;AGAST;KAZE;AKAZE" --Feature2D/2Descriptor "3:Brief;ORB;SIFT;SURF;BRISK;FREAK;KAZE;AKAZE;LUCID;LATCH;DAISY" --Camera/5mediaPath $FOLDER > "$FILE_NAME"_SURF.txt &

~/code/uw_image_enhance/find-object-0.6.0/find-object/bin/find_object --no_display --config ~/code/uw_image_enhance/find-object-0.6.0/find-object/custom/$CONFIG --Feature2D/1Detector "11:Dense;Fast;GFTT;MSER;ORB;SIFT;Star;SURF;BRISK;AGAST;KAZE;AKAZE" --Feature2D/2Descriptor "7:Brief;ORB;SIFT;SURF;BRISK;FREAK;KAZE;AKAZE;LUCID;LATCH;DAISY" --Camera/5mediaPath $FOLDER > "$FILE_NAME"_AKAZE.txt &
# --Feature2D/AKAZE_threshold 0.0001

~/code/uw_image_enhance/find-object-0.6.0/find-object/bin/find_object --no_display --config ~/code/uw_image_enhance/find-object-0.6.0/find-object/custom/$CONFIG --Feature2D/1Detector "8:Dense;Fast;GFTT;MSER;ORB;SIFT;Star;SURF;BRISK;AGAST;KAZE;AKAZE" --Feature2D/2Descriptor "4:Brief;ORB;SIFT;SURF;BRISK;FREAK;KAZE;AKAZE;LUCID;LATCH;DAISY" --Camera/5mediaPath $FOLDER > "$FILE_NAME"_BRISK.txt;


