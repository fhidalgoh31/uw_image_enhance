#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include <iostream>
#include <time.h>
using namespace cv;
using namespace std;
int main( int argc, char** argv )
{
    RNG rng(time(0));
    for(int loop=1;loop<31;loop++)
    {
        int blockSize=75;
        int imageSize=blockSize*7;
        int counter=0;
        Mat chessBoard(imageSize,imageSize,CV_8UC3,Scalar::all(0));
        unsigned char color=0;

        for(int i=0;i<imageSize;i=i+blockSize){
        
            for(int j=0;j<imageSize;j=j+blockSize){    
            // cout << "code:" << code << "color:" << code%2 << "\n";
            if (rng.uniform(0,2)) 
                color=255;
            else
                color=0;
            Mat ROI=chessBoard(Rect(i,j,blockSize,blockSize));
            ROI.setTo(Scalar::all(color));
            counter++;
        }
        }
        imshow("Chess board", chessBoard);
        char pngname[20];
        sprintf(pngname, "fig%d.png", loop);
        // string str = ss.str();
        imwrite(pngname, chessBoard);
        // waitKey(0);                                          // Wait for a keystroke in the window
    }
    return 0;
}