"""
========
Barchart
========

A bar plot with errorbars and height labels on individual bars
"""
import numpy as np
import matplotlib.pyplot as plt
from itertools import islice
import csv

csv_header=['Type','Dataset','Profile','Feature','avg','std']
graphs=['Features Detected','Inliers Detected','Processing time','Inliers ratio features','Inliers ratio matches','Processing time / Features']
descriptors=['SIFT','SURF','ORB','BRISK','AKAZE']

class Graphs:
    title = " "
    file_name=" "
    x_label="Frames"
    y_label=" "
    x=-1
    y=-1
    filter=0

My_Graphs = []

object_graph = Graphs()
# object_graph.title = "dummy"
# object_graph.file_name="dummy"
# object_graph.y_label="dummy"
# object_graph.x_label="dummy"
# object_graph.y=csv_header.index('frame')
# object_graph.x=csv_header.index('frame')
# object_graph.filter=10
# My_Graphs.append(object_graph)

object_graph = Graphs()
object_graph.title = "Features Detected"
object_graph.file_name="Features"
object_graph.y_label="Features"
object_graph.x_label="Datasets"
My_Graphs.append(object_graph)

object_graph = Graphs()
object_graph.title = "Inliers Detected"
object_graph.file_name="Inliers"
object_graph.y_label="Inliers"
object_graph.x_label="Datasets"
My_Graphs.append(object_graph)

object_graph = Graphs()
object_graph.title = "Processing time"
object_graph.file_name="p_time"
object_graph.y_label="time (ms)"
object_graph.x_label="Datasets"
My_Graphs.append(object_graph)

# object_graph = Graphs()
# object_graph.title = "Inliers ratio features"
# object_graph.file_name="stats_inliers_features"
# object_graph.y_label="inliers/detected features"
# object_graph.x_label="Datasets"
# My_Graphs.append(object_graph)

# object_graph = Graphs()
# object_graph.title = "Inliers ratio matches"
# object_graph.file_name="stats_inliers_matches"
# object_graph.y_label="inliers/matches"
# object_graph.x_label="Datasets"
# My_Graphs.append(object_graph)

# object_graph = Graphs()
# object_graph.title = "Processing time / Features"
# object_graph.file_name="stats_p_time_features"
# object_graph.y_label="time (ms)"
# object_graph.x_label="Datasets"
# My_Graphs.append(object_graph)




width=0.15
descriptors=['SIFT','SURF','AKAZE','ORB','BRISK']



# for ii in range(len(My_Graphs)):
for ii in range(len(My_Graphs)):
    fig, ax = plt.subplots(figsize=(10, 2.3), dpi=100)
    with open('my_stats.csv','r') as csvfile:
        data = islice(csv.reader(csvfile, delimiter=','),1,None)
        SURF_avg=[]
        SIFT_avg=[]
        AKAZE_avg=[]
        ORB_avg=[]
        BRISK_avg=[]
        SURF_std=[]
        SIFT_std=[]
        AKAZE_std=[]
        ORB_std=[]
        BRISK_std=[]
        for row in data:
            if str(graphs[ii]) in str(row[csv_header.index('Type')]):
                if str(descriptors[0]) in str(row[csv_header.index('Feature')]):
                    SIFT_avg.append(float(row[csv_header.index('avg')]))
                    SIFT_std.append(float(row[csv_header.index('std')]))
                elif str(descriptors[1]) in str(row[csv_header.index('Feature')]):
                    SURF_avg.append(float(row[csv_header.index('avg')]))
                    SURF_std.append(float(row[csv_header.index('std')]))
                elif str(descriptors[2]) in str(row[csv_header.index('Feature')]):
                    ORB_avg.append(float(row[csv_header.index('avg')]))
                    ORB_std.append(float(row[csv_header.index('std')]))
                elif str(descriptors[3]) in str(row[csv_header.index('Feature')]):
                    BRISK_avg.append(float(row[csv_header.index('avg')]))
                    BRISK_std.append(float(row[csv_header.index('std')]))
                elif str(descriptors[4]) in str(row[csv_header.index('Feature')]):
                    AKAZE_avg.append(float(row[csv_header.index('avg')]))
                    AKAZE_std.append(float(row[csv_header.index('std')]))

    # print np.size(SIFT_avg)
    # print np.size(SIFT_std)
    # print np.size(SURF_avg)
    # print np.size(SURF_std)
    # print np.size(ORB_avg)
    # print np.size(ORB_std)
    # print np.size(BRISK_avg)
    # print np.size(BRISK_std)
    # print np.size(AKAZE_avg)
    # print np.size(AKAZE_std)

    ind = np.arange(np.size(SIFT_avg))  # the x locations for the groups
    rects1 = ax.bar(ind+width*0, SIFT_avg, width, yerr=SIFT_std,alpha=0.8, ecolor='blue', capsize=2.5)
    rects2 = ax.bar(ind+width*1, SURF_avg, width, yerr=SURF_std,alpha=0.8, ecolor='blue', capsize=2.5)
    rects3 = ax.bar(ind+width*2, ORB_avg, width, yerr=ORB_std,alpha=0.8, ecolor='blue', capsize=2.5)
    rects4 = ax.bar(ind+width*3, BRISK_avg, width, yerr=BRISK_std,alpha=0.8, ecolor='blue', capsize=2.5)
    rects5 = ax.bar(ind+width*4, AKAZE_avg, width, yerr=AKAZE_std,alpha=0.8, ecolor='blue', capsize=2.5)

    plt.grid(which='major', linestyle='-', linewidth='0.5', color='grey')

    ax.set_xlabel(My_Graphs[ii].x_label)
    ax.set_ylabel(My_Graphs[ii].y_label)
    plt.title(My_Graphs[ii].title)
    plt.grid(b=True, which='minor', color='grey', linestyle='--', alpha=0.2)
    plt.minorticks_on()
    ax.set_xticks(ind + width*2)
    ax.set_xticklabels(('D1', 'D2', 'D3', 'D4', 'D5','D6','D7','D8'))

    plt.legend((rects1[0], rects2[0], rects3[0], rects4[0],rects5[0]), descriptors,bbox_to_anchor=(1.1, 1.05))
    fig.tight_layout()
    # plt.savefig('./sum_plots/' + My_Graphs[ii].file_name + '_' + input_file[:idx] +'.pdf')
    plt.show()
