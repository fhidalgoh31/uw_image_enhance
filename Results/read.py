import csv
import sys

def safe_div(x,y):
    if y == 0:
        return 0
    return x / y

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

csv_filename='log.csv'
input_file='log.txt'

if  len(sys.argv)>2:
    input_file=sys.argv[1]
    csv_filename=sys.argv[2]
elif len(sys.argv)>1:
    input_file=sys.argv[1]
    csv_filename=sys.argv[1]
    csv_filename=csv_filename[:-4]+'.csv'
else:
    print("usage read input_file output_file\n defaults: file.txt log.csv")

print (sys.argv[1])
descriptors=['AKAZE','ORB','SIFT','SURF','BRISK']
idx=input_file.rfind('_',0)
core_name=input_file[:idx+1]

for desc in descriptors: 
    with open (''.join([core_name + desc + '.txt']), 'rt') as in_file, open(''.join([core_name + desc + '.csv']), 'w', newline='') as csvfile:
        n=0
        printcsv=0
        scene=0
        frame="0"
        desc_obj="0"
        t_obj="0"
        desc_scn="0"
        t_scn="0"
        inliers="0"
        outliers="0"
        words="0"
        t_words="0"
    
        csv_file = csv.writer(csvfile, delimiter=',',quotechar='*', quoting=csv.QUOTE_MINIMAL)
        csv_file.writerow(['n' , 'frame' , 'desc_obj' , 't_obj' , 'words' , 't_words' , 'desc_scn' , 't_scn' , 'inliers' , 'outliers','in/desc','in/match', 't/features'])
        for line in in_file: # Store each line in a string variable "line"
            n=n+1
            idx=line.find("FHH Path:")
            if idx > 0:
                csv_file.writerow([line])
            # print(line) # prints that line
            idx=line.find("Extracting descriptors from object -1")
            if idx > 0:
                scene=1
            idx=line.find(" descriptors extracted from object")
            if idx > 0:
                idx2=line.find(" (")
                idx3=line.find(" ms")
                if scene==1:
                    desc_scn=line[12:idx]
                    t_scn=line[idx2+5:idx3]
                else:
                    desc_obj=line[12:idx]
                    t_obj=line[idx2+5:idx3]

            idx=line.find("words")
            if idx > 0:
                idx=line.find("descriptors (")
                idx2=line.find(" words,")
                idx3=line.find(" ms")
                words=line[idx+13:idx2]
                t_words=line[idx2+8:idx3]

            idx=line.find("Frame: ")
            if idx > 0:
                idx2=line.find(" Inliers")
                idx3=line.find(" Outliers")
                idx4=line.find("*")
                frame=line[idx+7:idx2]
                if (is_number(line[idx2+10:idx3])):
                    inliers=line[idx2+10:idx3]
                if (is_number(line[idx3+11:idx4])):
                    outliers=line[idx3+11:idx4]
                printcsv=1
            
            
            # if 'frame' in locals():
            #     print(n, idx, frame)
            #     printcsv=1
            if printcsv:
                if (is_number(frame) and is_number(desc_obj) and  is_number(t_obj) and is_number(desc_scn) and is_number(t_scn) and is_number(inliers) and is_number(words) and is_number(t_words)):
                    #print (n, frame)
                    csv_file.writerow([n] + [frame] + [desc_obj] + [t_obj] + [words] + [t_words] + [desc_scn] + [t_scn] + [inliers] + [outliers] + [str(round(100*safe_div(int(inliers),int(desc_obj)),2))] + [str(round(100*safe_div(int(inliers),int(inliers)+int(outliers)),2))] + [str(round(safe_div(int(t_obj),int(desc_obj)),2))])
                printcsv=0
                scene=0
                frame="-1"
                desc_obj="-1"
                t_obj="-1"
                desc_scn="-1"
                t_scn="-1"
                inliers="-1"
                outliers="-1"
                words="-1"
                t_words="-1"
