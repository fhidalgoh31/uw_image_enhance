# OPTION 1   OPTION 1  OPTION 1    OPTION 1    OPTION 1   OPTION 1   
# import matplotlib.pyplot as plt
# import numpy as np

# x = np.array([1, 2, 3, 4, 5])
# y = np.power(x, 2) # Effectively y = x**2
# e = np.array([1.5, 2.6, 3.7, 4.6, 5.5])

# plt.errorbar(x, y, e, linestyle='None', marker='^')

# plt.show()
# OPTION 1   OPTION 1  OPTION 1    OPTION 1    OPTION 1   OPTION 1   

# OPTION 2     OPTION 2     OPTION 2    OPTION 2

# import numpy as np
# import matplotlib.pyplot as plt

# # Create Arrays for the plot
# materials = ['Aluminum', 'Copper', 'Steel']
# x_pos = np.arange(len(materials))
# CTEs = [10, 10, 10]
# error = [5, 3, 1]


# # Build the plot
# fig, ax = plt.subplots()
# ax.bar(x_pos, CTEs, yerr=error, align='center', alpha=0.5, ecolor='black', capsize=10)
# ax.set_ylabel('Coefficient of Thermal Expansion ($\degree C^{-1}$)')
# ax.set_xticks(x_pos)
# ax.set_xticklabels(materials)
# ax.set_title('Coefficent of Thermal Expansion (CTE) of Three Metals')
# ax.yaxis.grid(True)

# # Save the figure and show
# plt.tight_layout()
# plt.savefig('bar_plot_with_error_bars.png')
# plt.show()

# OPTION 2     OPTION 2     OPTION 2    OPTION 2


"""
========
Barchart
========

A bar plot with errorbars and height labels on individual bars
"""
import numpy as np
import matplotlib.pyplot as plt

N = 7
width = 0.10       # the width of the bars
ind = np.arange(N)  # the x locations for the groups
fig, ax = plt.subplots(figsize=(15, 2.3), dpi=100)

men_means = (20, 35, 30, 35,10,20)
a=5
men_means= men_means + (a,)
men_std = (2, 3, 4, 1, 2,1,2)

rects1 = ax.bar(ind, men_means, width, yerr=men_std,alpha=0.8, ecolor='blue', capsize=2.5)

women_means = (25, 32, 34, 20, 25, 10,5)
women_std = (3, 5, 2, 3, 3, 2,.5)
rects2 = ax.bar(ind + width, women_means, width, yerr=women_std)

xxx_means = (20, 15, 10, 20, 25, 10 ,20)
xxx_std = (3, 5, 2, 1, 1, 5 ,6)
rects3 = ax.bar(ind + width*2, xxx_means, width, yerr=xxx_std)

# xxx_means = (20, 15, 10, 20, 25)
# xxx_std = (3, 5, 2, 1, 1)
# rects4 = ax.bar(ind + width*2, xxx_means, width, yerr=xxx_std)

# add some text for labels, title and axes ticks
ax.set_ylabel('Scores')
ax.set_title('Scores by group and gender')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))

# ax.legend((rects1[0], rects2[0], rects3[0],rects4[0]), ('Men', 'Women', 'xxx', 'yyy'))


# def autolabel(rects):
#     """
#     Attach a text label above each bar displaying its height
#     """
#     for rect in rects:
#         height = rect.get_height()
#         ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
#                 '%d' % int(height),
#                 ha='center', va='bottom')

# autolabel(rects1)
# autolabel(rects2)
# autolabel(rects3)

plt.show()