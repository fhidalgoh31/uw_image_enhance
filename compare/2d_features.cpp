#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/cuda.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <vector>
#include <iostream>
#include <iomanip>
#include "utils.h" // Drawing and printing functions stats
#include "im_tools.hpp" 

// #include <QtCore/QTime>
// #include <QtCore/QTimer>

using namespace std;
// using namespace cv;

int interval=1;

#define fps 2
#define delay 1000/fps

const double akaze_thresh = 3e-4; // AKAZE detection threshold set to locate about 1000 keypoints
const double ransac_thresh = 2.5f; // RANSAC inlier threshold
const double nn_match_ratio = 0.8f; // Nearest-neighbour matching ratio
const int bb_min_inliers = 100; // Minimal number of inliers to draw bounding box
const int stats_update_period = 1; // On-screen statistics are updated every 10 frames
class Tracker
{
public:
    Tracker(cv::Ptr<Feature2D> _detector, cv::Ptr<DescriptorMatcher> _matcher) :
        detector(_detector),
        matcher(_matcher)
    {}
    void setFirstFrame(const cv::Mat frame, vector<Point2f> bb, string title, Stats& stats);
    Mat process(const cv::Mat frame, Stats& stats);
    cv::Ptr<Feature2D> getDetector() {
        return detector;
    }
protected:
    cv::Ptr<Feature2D> detector;
    cv::Ptr<DescriptorMatcher> matcher;
    cv::Mat first_frame, first_desc;
    vector<KeyPoint> first_kp;
    vector<Point2f> object_bb;
};
void Tracker::setFirstFrame(const cv::Mat frame, vector<Point2f> bb, string title, Stats& stats)
{
    std::cout << "iiiiiiiiiiiiiiiiiiiiiiiiiii Setfirstframe start" << std::endl ;
    first_frame = frame.clone();
    detector->detectAndCompute(first_frame, noArray(), first_kp, first_desc);
    stats.keypoints = (int)first_kp.size();
    drawBoundingBox(first_frame, bb);
    putText(first_frame, title, Point(0, 60), FONT_HERSHEY_PLAIN, 5, Scalar::all(0), 4);
    object_bb = bb;
    std::cout << "iiiiiiiiiiiiiiiiiiiiiiiiiii firstframe end 1" << std::endl ;
}
Mat Tracker::process(const Mat frame, Stats& stats)
{
    vector<KeyPoint> kp;
    cv::Mat desc;
    cv::Ptr<DescriptorExtractor> extractor;
    detector->detectAndCompute(frame, noArray(), kp, desc);
    stats.keypoints = (int)kp.size();
    vector< vector<DMatch> > matches;
    vector<KeyPoint> matched1, matched2;
    std::cout << "iiiiiiiiiiiiiiiiiiiiiiiiiii matcher first_desc:" << first_desc.size() << std::endl << "desc: " << desc.size() << std::endl ;
    matcher->knnMatch(first_desc, desc, matches, 2);
    for(unsigned i = 0; i < matches.size(); i++) {
        if(matches[i][0].distance < nn_match_ratio * matches[i][1].distance) {
            matched1.push_back(first_kp[matches[i][0].queryIdx]);
            matched2.push_back(      kp[matches[i][0].trainIdx]);
        }
    }
    std::cout << "iiiiiiiiiiiiiiiiiiiiiiiiiii matcher matched 1" << matches.size() << std::endl << "matches: " << matched2[1].pt << std::endl ;

    // drawKeypoints( first_frame, first_kp, first_frame, Scalar::all(-1),
    //              DrawMatchesFlags::DRAW_RICH_KEYPOINTS );

    stats.matches = (int)matched1.size();
    cv::Mat inlier_mask, homography;
    vector<KeyPoint> inliers1, inliers2;
    vector<DMatch> inlier_matches;
    if(matched1.size() >= 4) {
        homography = cv::findHomography(Points(matched1), Points(matched2),
                                    RANSAC, ransac_thresh, inlier_mask);
    }
    if(matched1.size() < 4 || homography.empty()) {
        Mat res;
        hconcat(first_frame, frame, res);
        stats.inliers = 0;
        stats.ratio = 0;
        return res;
    }
    for(unsigned i = 0; i < matched1.size(); i++) {
        if(inlier_mask.at<uchar>(i)) {
            int new_i = static_cast<int>(inliers1.size());
            inliers1.push_back(matched1[i]);
            inliers2.push_back(matched2[i]);
            inlier_matches.push_back(DMatch(new_i, new_i, 0));
        }
    }
    stats.inliers = (int)inliers1.size();
    stats.ratio = stats.inliers * 1.0 / stats.matches;
    vector<Point2f> new_bb;
    perspectiveTransform(object_bb, new_bb, homography);
    cv::Mat frame_with_bb = frame.clone();
    if(stats.inliers >= bb_min_inliers) {
        drawBoundingBox(frame_with_bb, new_bb);
    }
    cv::Mat res;
    // drawMatches(first_frame, inliers1, frame_with_bb, inliers2,
    //             inlier_matches, res,
    //             Scalar(255, 0, 0), Scalar(255, 255, 255));

    drawMatches(first_frame, first_kp, frame_with_bb, kp,
                inlier_matches, res,
                Scalar(255, 0, 0), Scalar(255, 255, 255));

    return res;
}

int main(int argc, char **argv)
{
    // time.start();
    if(argc < 4) {
        cerr << "Usage: " << endl <<
                "akaze_track input_path output_path bounding_box" << endl;
        return 1;
    }
    // Retrieve paths to images
    vector<string> vstrImageFilenames;
    vector<double> vTimestamps;
    int beg_Images, end_Images;

    string strFile = string(argv[1])+"/rgb.txt";
    LoadImages(strFile, vstrImageFilenames, vTimestamps);
	int nImages = vstrImageFilenames.size();

    vector<Point2f> bb;
    FileStorage fs(argv[3], FileStorage::READ);
    if(fs["bounding_box"].empty()) {
        cerr << "Couldn't read bounding_box from " << argv[3] << endl;
        return 1;
    }
    fs["bounding_box"] >> bb;

    for(int i=4; i<argc; ++i)
	{
		if(strcmp(argv[i], "-begin") == 0 ||
		   strcmp(argv[i], "--begin") == 0 ||
		   strcmp(argv[i], "-b") == 0 ||
		   strcmp(argv[i], "--b") == 0)
		{
			++i;
            if(i < argc)
				beg_Images=atoi(argv[i]);
        }

        if(strcmp(argv[i], "-end") == 0 ||
		   strcmp(argv[i], "--end") == 0 ||
		   strcmp(argv[i], "-e") == 0 ||
		   strcmp(argv[i], "--e") == 0)
		{
			++i;
            if(i < argc)
				end_Images=atoi(argv[i]);
        }
    }
    cv::Mat frame;
    int ni=0;
    if (beg_Images>1)
        {ni=beg_Images; ni++;}
    frame = cv::imread(string(argv[1])+"/"+vstrImageFilenames[ni],cv::IMREAD_COLOR );   // for Images

    cv::VideoWriter  video_out(argv[2],
                        VideoWriter::fourcc('D','I','V','X'),
                        fps,
                        Size(2 * frame.cols,
                            2 * frame.rows));

    if(!video_out.isOpened()) {
        cerr << "Couldn't open " << argv[2] << endl;
        return 1;
    }

    //End Parameters
    
    // Program
    Stats stats, akaze_stats, orb_stats;
    vector<Stats> stats_log_akaze, stats_log_ORB;
    cv::Ptr<AKAZE> akaze = AKAZE::create();
    akaze->setThreshold(akaze_thresh);
    Ptr<ORB> orb = ORB::create();
    // cv::Ptr<xfeatures2d::SIFT> orb = xfeatures2d::SIFT::create();
    // static Ptr<ORB> cv::ORB::create 	( 	int  	nfeatures = 500,
	// 	float  	scaleFactor = 1.2f,
	// 	int  	nlevels = 8,
	// 	int  	edgeThreshold = 31,
	// 	int  	firstLevel = 0,
	// 	int  	WTA_K = 2,
	// 	int  	scoreType = ORB::HARRIS_SCORE,
	// 	int  	patchSize = 31,
	// 	int  	fastThreshold = 20 
	// ) 		
    // orb->setMaxFeatures(stats.keypoints);
    // orb->setMaxFeatures(1000);
    cv::Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce-Hamming");
    Tracker akaze_tracker(akaze, matcher);
    Tracker orb_tracker(orb, matcher);

    Stats akaze_draw_stats, orb_draw_stats;
    cv::Mat akaze_res, orb_res, res_frame;

    for(ni; ni<nImages; ni+=interval){
        akaze_tracker.setFirstFrame(frame, bb, "AKAZE", stats);
        orb_tracker.setFirstFrame(frame, bb, "ORB", stats);

        frame = cv::imread(string(argv[1])+"/"+vstrImageFilenames[ni],cv::IMREAD_COLOR );   // for Images
        bool update_stats = (ni % stats_update_period == 0);
        std::cout << "iiiiiiiiiiiiiiiiiiiiiiiiiii Here 1" << std::endl ;
        akaze_res = akaze_tracker.process(frame, stats);
        akaze_stats += stats;
        stats_log_akaze.push_back(stats);
        std::cout << "iiiiiiiiiiiiiiiiiiiiiiiiiii Here 2" << std::endl ;
        if(update_stats) {
            akaze_draw_stats = stats;
        }
        orb->setMaxFeatures(stats.keypoints);
        orb->setMaxFeatures(1000);
        std::cout << "iiiiiiiiiiiiiiiiiiiiiiiiiii Here 3" << std::endl ;
        orb_res = orb_tracker.process(frame, stats);
        std::cout << "iiiiiiiiiiiiiiiiiiiiiiiiiii Here 4" << std::endl ;
        orb_stats += stats;
        stats_log_ORB.push_back(stats);
        if(update_stats) {
            orb_draw_stats = stats;
        }
        drawStatistics(akaze_res, akaze_draw_stats);
        drawStatistics(orb_res, orb_draw_stats);
        vconcat(akaze_res, orb_res, res_frame);
        video_out << res_frame;
        cout << ni << "/" << nImages - 1 << endl;

        // if(waitKey(delay) >= 0)
		// 	if(waitKey(0) == 27)
		// 		break;

		if (ni>end_Images)
			{
				std::cout << "Pause, Press enter to finish" << std::endl;
				if((char)waitKey(0) == 13) std::cout << "Enter pressed" << std::endl;  // Pause ZZZ
				break;
			}
    }
    akaze_stats /= nImages - 1;
    orb_stats /= nImages - 1;
    printStatistics("AKAZE", akaze_stats);
    printStatistics("ORB", orb_stats);
    cout << "Log stores " << int(stats_log_akaze.size()) << " numbers.\n";
    return 0;
}
