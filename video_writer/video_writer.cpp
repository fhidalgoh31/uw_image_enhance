#include <opencv2/features2d.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <iomanip>
using namespace std;


int main(int argc, char **argv)
{

    // Load input video
    cv::VideoCapture input_cap(argv[1]);
    if (!input_cap.isOpened())
    {
        std::cout << "!!! Input video could not be opened" << std::endl;
        return 1;
    }

    cout<< (int)input_cap.get(CV_CAP_PROP_FOURCC);
    cv::VideoWriter output_cap;
    output_cap.open(argv[2], 
                cv::VideoWriter::fourcc('D','I','V','X'),
                20,
                cv::Size(input_cap.get(CV_CAP_PROP_FRAME_WIDTH),
                input_cap.get(CV_CAP_PROP_FRAME_HEIGHT)));
    if (!output_cap.isOpened())
    {
            std::cout << "!!! Output video could not be opened" << std::endl;
            return 1;
    }


    // Loop to read from input and write to output
    cv::Mat frame;

    for (int i=0; i<60; i++)
    {       
        if (!input_cap.read(frame))             
            break;

        output_cap.write(frame);
    }

    input_cap.release();
    output_cap.release();

    return 0;
}