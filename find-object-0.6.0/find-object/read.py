import csv
import sys
csv_filename='log.csv'
input_file='file.txt'
if  len(sys.argv)>2:
    csv_filename=sys.argv[2]
    input_file=sys.argv[1]
else:
    print("usage read input_file output_file\n defaults: file.txt log.csv")

with open (input_file, 'rt') as in_file, open(csv_filename, 'w', newline='') as csvfile:
    n=0
    printcsv=0
    scene=0
    frame="0"
    desc_obj="0"
    t_obj="0"
    desc_scn="0"
    t_scn="0"
    inliers="0"
    outliers="0"
    words="0"
    t_words="0"
# Words
# time vocabulary

    csv_file = csv.writer(csvfile, delimiter=',',quotechar='*', quoting=csv.QUOTE_MINIMAL)
    csv_file.writerow(['n' , 'frame' , 'desc_obj' , 't_obj' , 'words' , 't_words' , 'desc_scn' , 't_scn' , 'inliers' , 'outliers'])
    for line in in_file: # Store each line in a string variable "line"
        n=n+1
        idx=line.find("FHH Path:")
        if idx > 0:
            csv_file.writerow([line])
        # print(line) # prints that line
        idx=line.find("Extracting descriptors from object -1")
        if idx > 0:
            scene=1
        idx=line.find(" descriptors extracted from object")
        if idx > 0:
            idx2=line.find(" (")
            idx3=line.find(" ms")
            if scene==1:
                desc_scn=line[12:idx]
                t_scn=line[idx2+5:idx3]
            else:
                desc_obj=line[12:idx]
                t_obj=line[idx2+5:idx3]

        idx=line.find("words")
        if idx > 0:
            idx=line.find("descriptors (")
            idx2=line.find(" words,")
            idx3=line.find(" ms")
            words=line[idx+13:idx2]
            t_words=line[idx2+8:idx3]

        idx=line.find("Frame: ")
        if idx > 0:
            idx2=line.find(" Inliers")
            idx3=line.find(" Outliers")
            idx4=line.find("*")
            frame=line[idx+7:idx2]
            inliers=line[idx2+10:idx3]
            outliers=line[idx3+11:idx4-1]
            printcsv=1
           
           
        # if 'frame' in locals():
        #     print(n, idx, frame)
        #     printcsv=1
        if printcsv:
            print (n, frame)
            csv_file.writerow([n] + [frame] + [desc_obj] + [t_obj] + [words] + [t_words] + [desc_scn] + [t_scn] + [inliers] + [outliers])
            printcsv=0
            scene=0
            frame="-1"
            desc_obj="-1"
            t_obj="-1"
            desc_scn="-1"
            t_scn="-1"
            inliers="-1"
            outliers="-1"
            words="-1"
            t_words="-1"


#     # spamwriter.writerow(['Spam'] * 5 + ['Baked Beans'])
#     # spamwriter.writerow(['Spam', 'Lovely Spam', 'Wonderful Spam'])
   
# with open("hello.txt", "w") as f: 
#     f.write("Hello World") 
#     f.write("Hello World") 
#     f.write("\nHello World")
    

# // // // //Variables
# Frame   x 
# Features object  x 
# Time Object descriptors   x 
# Words
# time vocabulary
# Features Scene   x 
# Time Scene descriptors   x
# Inliers
