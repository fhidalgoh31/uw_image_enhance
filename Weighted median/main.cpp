#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/ximgproc.hpp>
// using namespace cv;

int main(int argc, char** argv )
{
    if ( argc != 2 )
    {
        printf("usage: DisplayImage.out <Image_Path>\n");
        return -1;
    }

    cv::Mat image;
    image = cv::imread( argv[1], 1 );
    // ximgproc::weightedMedianFilter 	( 	InputArray  	joint,
	// 	InputArray  	src,
	// 	OutputArray  	dst,
	// 	int  	r,
	// 	double  	sigma = 25.5,
	// 	int  	weightType = WMF_EXP,
	// 	InputArray  	mask = noArray() 
	// ) 	

    //cv::Mat mask = cv::Mat::zeros( leftStereo.size(), CV_8U ); // bad!
    cv::Mat mask = cv::Mat::ones( image.size(), CV_8U ); // good!
    //cv::Mat mask;
    //cv::threshold( disparity8, mask, 0, 1, cv::THRESH_BINARY ); // bad!
    cv::Mat filtered;
    // cv::ximgproc::weightedMedianFilter( image, image, filtered, 3, 20, cv::ximgproc::WMFWeightType::WMF_EXP, mask );
    cv::ximgproc::weightedMedianFilter( image, image, filtered, 10, 100,cv::ximgproc::WMF_EXP, mask );

    if ( !image.data )
    {
        printf("No image data \n");
        return -1;
    }
    cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE );
    cv::imshow("Display Image", image);

    cv::waitKey(0);

    return 0;
}