import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
import csv
import sys
import numpy as np
from itertools import islice
from cycler import cycler
# Two scales:
# https://matplotlib.org/gallery/api/two_scales.html
# from: https://www.swharden.com/wp/2008-11-17-linear-data-smoothing-in-python/
def smoothList(list,strippedXs=False,degree=10):  

    if strippedXs==True: return Xs[0:-(len(list)-(len(list)-degree+1))]  

    smoothed=[0]*(len(list)-degree+1)  

    for i in range(len(smoothed)):  

        smoothed[i]=sum(list[i:i+degree])/float(degree)  

    return smoothed  

class Graphs:
    title = " "
    file_name=" "
    x_label="Frames"
    y_label=" "
    x=-1
    y=-1
    filter=0

descriptors=['SIFT','SURF','ORB','BRISK','AKAZE']
csv_header=['n' , 'frame' , 'desc_obj' , 't_obj' , 'words' , 't_words' , 'desc_scn' , 't_scn' , 'inliers' , 'outliers','in/desc','in/match', 't/features']

if len(sys.argv)>4:
    print("usage read input_file output_file\n defaults: file.txt log.csv")
    sys.exit()

ini=4
endi=None
if len(sys.argv)>1:
    input_file=sys.argv[1]
if len(sys.argv)>2:
    ini = int(sys.argv[2])
if len(sys.argv)>3:
    endi= int(sys.argv[3])
    

My_Graphs = []

object_graph = Graphs()
object_graph.title = "dummy"
object_graph.file_name="dummy"
object_graph.y_label="dummy"
object_graph.x_label="dummy"
object_graph.y=csv_header.index('frame')
object_graph.x=csv_header.index('frame')
object_graph.filter=10
My_Graphs.append(object_graph)

object_graph = Graphs()
object_graph.title = "Features Detected"
object_graph.file_name="Features"
object_graph.y_label="Features"
object_graph.x_label="Frames"
object_graph.y=csv_header.index('desc_obj')
object_graph.x=csv_header.index('frame')
object_graph.filter=10
My_Graphs.append(object_graph)

object_graph = Graphs()
object_graph.title = "Inliers Detected"
object_graph.file_name="Inliers"
object_graph.y_label="Inliers"
object_graph.x_label="Frames"
object_graph.y=csv_header.index('inliers')
object_graph.x=csv_header.index('frame')
object_graph.filter=10
My_Graphs.append(object_graph)

object_graph = Graphs()
object_graph.title = "Processing time"
object_graph.file_name="p_time"
object_graph.y_label="time (ms)"
object_graph.x_label="Frame"
object_graph.y=csv_header.index('t_obj')
object_graph.x=csv_header.index('frame')
object_graph.filter=100
My_Graphs.append(object_graph)

object_graph = Graphs()
object_graph.title = "Inliers ratio features"
object_graph.file_name="stats_inliers_features"
object_graph.y_label="inliers/detected features"
object_graph.x_label="Frame"
object_graph.y=csv_header.index('in/desc')
object_graph.x=csv_header.index('frame')
object_graph.filter=100
My_Graphs.append(object_graph)

object_graph = Graphs()
object_graph.title = "Inliers ratio matches"
object_graph.file_name="stats_inliers_matches"
object_graph.y_label="inliers/matches"
object_graph.x_label="Frame"
object_graph.y=csv_header.index('in/match')
object_graph.x=csv_header.index('frame')
object_graph.filter=100
My_Graphs.append(object_graph)

object_graph = Graphs()
object_graph.title = "Processing time / Features"
object_graph.file_name="stats_p_time_features"
object_graph.y_label="time (ms)"
object_graph.x_label="Frame"
object_graph.y=csv_header.index('t/features')
object_graph.x=csv_header.index('frame')
object_graph.filter=100
My_Graphs.append(object_graph)

idx=input_file.rfind('_',0)
core_name=input_file[:idx+1]

for ii in range(len(My_Graphs)):
    fig, ax1 = plt.subplots(figsize=(15, 2.3), dpi=100)
    # plt.setp(ax1, linewidth=2, color='r')  # line1 is thick and red
    for desc in descriptors: 
        with open(''.join([core_name + desc + '.csv']),'r') as csvfile, open('stats.csv','a') as stat_file:
            plots = islice(csv.reader(csvfile, delimiter=','),ini,endi)
            # next(plots, None)  # skip the headers
            # next(plots, None)  # skip the headers
            # next(plots, None)  # skip the headers
            # next(plots, None)  # skip the headers
            x = []
            y = []
            for row in plots:
                x.append(float(row[My_Graphs[ii].x]))
                y.append(float(row[My_Graphs[ii].y]))
            # linestyle_cycler = cycler('linestyle',['-','--',':','-.'])
            plt.gcf().subplots_adjust(top=0.15)
            plt.rc('axes', prop_cycle=(cycler('color', ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd']) + cycler('linestyle', ['-', '--', ':', '-.','--'])))
            # plt.rc('axes', prop_cycle=linestyle_cycler)
            ax1.plot(smoothList(x,degree=My_Graphs[ii].filter),smoothList(y,degree=My_Graphs[ii].filter), label=desc, linewidth=1.5)
            file.write(stat_file,''.join([My_Graphs[ii].title +',' + core_name +','+ desc + ',' + 'avg,' + str(round(np.average(y),2)) + ',std,' + str(round(np.std(y),2))+ '\n']))
            # plt.text(0.5, 0.5, 'matplotlib', horizontalalignment='center', verticalalignment='center', transform=ax1.transAxes)
            # ax.annotate('annotate', xy=(2, 1), xytext=(3, 4),arrowprops=dict(facecolor='black', shrink=0.05))
            # ax1.annotate('annotate', xy=(500, -100))
            
    
    # Customize the major grid
    plt.grid(which='major', linestyle='-', linewidth='0.5', color='grey')
    # Customize the minor grid
    # plt.grid(which='minor', linestyle=':', linewidth='0.5', color='black')

    ax1.set_xlabel(My_Graphs[ii].x_label)
    ax1.set_ylabel(My_Graphs[ii].y_label)
    fig.tight_layout()
    plt.legend(loc="upper right")
    plt.title(My_Graphs[ii].title)
    plt.grid(b=True, which='minor', color='grey', linestyle='--', alpha=0.2)
    plt.minorticks_on()
    plt.savefig('./plots/' + My_Graphs[ii].file_name + '_' + input_file[:idx] +'.pdf')
    # plt.show()
