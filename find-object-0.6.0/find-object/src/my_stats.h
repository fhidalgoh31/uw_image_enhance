#include <stdio.h>

#ifndef MY_STATS_H_
#define MY_STATS_H_
// namespace find_object {
class my_stats
{
	public:
	int frame;
	int inliers;
	int getframe() {return frame;} ;

	void clear_vars()
	{
		frame=0; 
		inliers=0;
	}
	void print_vars()
	{
		printf("[STATS] %d,%d\n",frame,inliers);
	}
};
// }
#endif /* MY_STATS_H_ */